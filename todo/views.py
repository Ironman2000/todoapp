from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Todo
from .forms import TodoForm

def home_page(request):
	tasks = Todo.objects.all()
	return render(request, 'index.html', {'tasks': tasks})

def add_task(request):
	form = TodoForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()
		return redirect('home_page')

	return render(request, 'add.html', {'form': form})

def edit_task(request, id):
	task = get_object_or_404(Todo, pk=id)
	form = TodoForm(request.POST or None, instance=task)

	if form.is_valid():
		form.save()
		return redirect('home_page')

	return render(request, 'edit.html', {'task': task, 'form': form})

def delete_task(request, id):
	task = get_object_or_404(Todo, pk=id)

	if request.method == 'POST':
		task.delete()
		return redirect('home_page')

	return render(request, 'delete.html', {'task': task})
