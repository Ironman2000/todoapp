from django.urls import path
from .views import home_page, add_task, edit_task, delete_task

urlpatterns = [
    path('', home_page, name="home_page"),
    path('add/', add_task, name="add_task"),
    path('edit/<int:id>/', edit_task, name="edit_task"),
    path('delete/<int:id>/', delete_task, name="delete_task"),
]
